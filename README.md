#This is a Match Three Game using [Game Closure SDK](http://gameclosure.com).

A detailed explanation of the game code can be found in the
[Game Walk-Through Guide](http://docs.gameclosure.com/guide/game-walkthrough.html).

##Prerequisites:
You need devkit installed to run this game locally. Please follow the devkit
installation instructions.


##Quick Start:

Clone this game:
`git clone git clone https://tiendv@bitbucket.org/tiendv/tien-dinh-2015.git gemswappers`
Change into the game directory:
`cd gemswappersv2`

Add Devkit to the project (and register with the simulator)
`devkit install`

That's it! Run the simulator and open the GemSwappers project.