import animate;
import ui.View;
import ui.ImageView;
import ui.resource.Image as Image;

var gemImg1 = new Image({url : "resources/images/icon_block_01.png"});
var gemImg2 = new Image({url : "resources/images/icon_block_02.png"});
var gemImg3 = new Image({url : "resources/images/icon_block_03.png"});
var gemImg4 = new Image({url : "resources/images/icon_block_04.png"});
var gemImg5 = new Image({url : "resources/images/icon_block_05.png"});
var gemAniImg1 = new Image({url : "resources/images/shine_icon_0001.png"});
var gemAniImg2 = new Image({url : "resources/images/shine_icon_0002.png"});
var gemAniImg3 = new Image({url : "resources/images/shine_icon_0003.png"});
var gemAniImg4 = new Image({url : "resources/images/shine_icon_0004.png"});
var gemAniImg5 = new Image({url : "resources/images/shine_icon_0005.png"});
var gemAniImg6 = new Image({url : "resources/images/shine_icon_0006.png"});

var popAniImg1 = new Image({url : "resources/images/block_01_pop_0001.png"});
var popAniImg2 = new Image({url : "resources/images/block_01_pop_0002.png"});
var popAniImg3 = new Image({url : "resources/images/block_01_pop_0003.png"});

var COL_MAX = 7;
var ROW_MAX = 7;
var col_offset = 20;
var row_offset = 405;

exports = Class(ui.View, function (supr) {
	
	this.init = function(opts) {
		opts = merge(opts, {
			width:	99,
			height: 99
		});
		
		supr(this, 'init', [opts]);
		this._imgArr = [gemImg1, gemImg2, gemImg3, gemImg4, gemImg5];
		this._popAniArr = [popAniImg1, popAniImg2, popAniImg3];
	};
	
	this.setType = function (type) {
		this._type = type;
		
		this.build();	
		return this;
	};
	
	this.setPosition = function(row, col) {
		this._col = col;
		this._row = row;
	};
	
	this.build = function() {
		this._gemImgView = new ui.ImageView({
			superview: this,
			image: gemImg1,
			x: 0,
			y: 0,
			width: 99,
			height: 99
		});
		
		this._gemImgView.setImage(this._imgArr[this._type - 1]);
		
		this._gemAniImgView = new ui.ImageView({
			superview: this,
			image: gemAniImg1,
			x: 0,
			y: 0,
			width: 99,
			height: 99
		});
		
		this._popAniImgView = new ui.ImageView({
			superview: this,
			image: "resources/images/block_01_pop_0001.png",
			x: 0,
			y: 0,
			width: 99,
			height: 99
		});
		this._popAniImgView.style.visible = false;
		
		/* Create an animator object for idle gem.
		 */
		var count = 0;
		this._interval = setInterval(bind(this, function() {
			if (count % 20 == 0) {
				if (this._gemAniImgView.style.visible == false)
					this._gemAniImgView.style.visible = true;
				this._gemAniImgView.setImage(gemAniImg1);
			} else if (count % 20 == 1){
				this._gemAniImgView.setImage(gemAniImg2);
			} else if (count % 20 == 2){
				this._gemAniImgView.setImage(gemAniImg3);
			} else if (count % 20 == 3){
				this._gemAniImgView.setImage(gemAniImg4);
			} else if (count % 20 == 4){
				this._gemAniImgView.setImage(gemAniImg5);
			} else if (count % 20 == 5){
				this._gemAniImgView.setImage(gemAniImg6);
			} else {
				if (this._gemAniImgView.style.visible == true)
					this._gemAniImgView.style.visible = false;
			}
			count++;
		}), 99);
		
		//this._animator = animate(this._gemImgView);		
		this._animator = animate(this);		
		
		//this._inputview.on("InputStart", bind (this, function (evt) {
		this.on("InputStart", bind (this, function (evt) {
				//console.log("Input Start: " + this._row + this._col);
				//this._inputview.startDrag({
				this.startDrag({
				inputStartEvt: evt,
				radius: 5
				});
			}));
		
		//this._inputview.on('DragStart', bind(this, function (dragEvt) {
		this.on('DragStart', bind(this, function (dragEvt) {
			//console.log("DragStart");
				 // this._inputview.style.x = dragEvt.srcPt.x;
				  //this._inputview.style.y = dragEvt.srcPt.y;
			}));
			
		//this._inputview.on('Drag', bind(this, function (startEvt, dragEvt, delta) {
		this.on('Drag', bind(this, function (startEvt, dragEvt, delta) {
            var X = startEvt.srcPt.x - dragEvt.srcPt.x;
            var Y = startEvt.srcPt.y - dragEvt.srcPt.y;
            var direction = 0; // 1: E, 2: S, 3: W, 4: N
			if (X * X > Y * Y) {
				if (X > 0) direction = 3;
				else direction = 1;
			} else {
				if (Y > 0) direction = 4;
				else direction = 2;
			}
			
			var bShouldSwap = true;
			if (direction == 1) {
				if (this._col + 1 >= COL_MAX)
					bShouldSwap = false;
			} else if (direction == 2) {
				if (this._row + 1 >= ROW_MAX)
					bShouldSwap = false;
			} else if (direction == 3) {
				if (this._col - 1 < 0) 
					bShouldSwap = false;
			} else if (direction ==4) {
				if (this._row - 1 < 0)
					bShouldSwap = false;
			}
			
			if (bShouldSwap) {
				//console.log("swap :" + this._row + " " + this._col + " " + direction);
				this._gamescreen.emit('gem:swap', direction, this._col, this._row);
			}			
		}));
		
		//this._inputview.on('DragStop', bind(this, function (dragEvt) {
		this.on('DragStop', bind(this, function (dragEvt) {
			//console.log("Drag stop");
			  //this._inputview.style.x = 0;
			  //this._inputview.style.y = 0;
			  
			  this._gamescreen.emit('gem:drag_stop');
		}));		
	};
	
	this.Move = function(valrow, valcol, direction, shouldEmit, isInverse) {
		//console.log("move: " + this._row + " " + this._col);
		//this._animator.now({x: this._gemImgView.style.x + valcol, y: this._gemImgView.style.y + valrow}, 300).then(bind(this, function() {
		this._animator.now({x: this.style.x + valcol, y: this.style.y + valrow}, 300).then(function() {
			if (shouldEmit) 
				this._gamescreen.emit('gem:swap_done', direction, this._col, this._row);
			if (isInverse) 
				this._gamescreen.emit('gem:swap_back_finish');
		});
	}
	
	this.Reset = function() {
		this.style.x = this._offsetX;
		this.style.y = this._offsetY;
		
		this._gemImgView.setImage(this._imgArr[this._type - 1]);
	}
	
	this.setOffset = function (x, y) {
		this.style.x = x;
		this.style.y = y;
		this._offsetX = x;
		this._offsetY = y;
	}
	
	this.Disappear = function() {
		this._gemImgView.style.visible = false;
		
		// Clear animation
		clearInterval(this._interval);
		if (this._gemAniImgView.style.visible == true)
					this._gemAniImgView.style.visible = false;
				
		var count = 0;
		this._interval = setInterval(bind(this, function() {
			if (count > 2) {
				if (this._popAniImgView.style.visible == true)
					this._popAniImgView.style.visible = false;
				
				clearInterval(this._interval);
				this.removeFromSuperview();
				this._gamescreen.emit("gem:disappear", this);
			} else {
				if (this._popAniImgView.style.visible == false)
					this._popAniImgView.style.visible = true;
				this._popAniImgView.setImage(this._popAniArr[count]);
			}
			count++;
		}), 40);
	};
	
	this.setGameScreen = function(gamescreen) {
		this._gamescreen = gamescreen;
	}
	
	this.Fall = function(valrow, col, shouldEmitCol, shouldEmitRow) {
		clearInterval(this._interval);
		this._interval = setInterval(bind(this, function() {
			if (this.style.y + this.style.width > row_offset) {
				if (this.style.visible == false)
					this.style.visible = true;
			}
		}), 20);
		this._animator.now({y: this.style.y + valrow}, 1000, animate.easeInCubic).then( bind(this, function() {
			clearInterval(this._interval);
			if (shouldEmitCol) {
				//console.log("Emit the falling function: " + this._offsetY);
				this.setOffset(this.style.x, this.style.y);
				if (shouldEmitRow)
					this._gamescreen.emit("gamescreen:gem_fall_finish", col, true);
				else 
					this._gamescreen.emit("gamescreen:gem_fall_finish", col, false);
			}
			
			var count = 0;
			this._interval = setInterval(bind(this, function() {
				if (count % 20 == 0) {
					if (this._gemAniImgView.style.visible == false)
						this._gemAniImgView.style.visible = true;
					this._gemAniImgView.setImage(gemAniImg1);
				} else if (count % 20 == 1){
					this._gemAniImgView.setImage(gemAniImg2);
				} else if (count % 20 == 2){
					this._gemAniImgView.setImage(gemAniImg3);
				} else if (count % 20 == 3){
					this._gemAniImgView.setImage(gemAniImg4);
				} else if (count % 20 == 4){
					this._gemAniImgView.setImage(gemAniImg5);
				} else if (count % 20 == 5){
					this._gemAniImgView.setImage(gemAniImg6);
				} else {
					if (this._gemAniImgView.style.visible == true)
						this._gemAniImgView.style.visible = false;
				}
				count++;
			}), 99);			
		}));
	}
});