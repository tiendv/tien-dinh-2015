/*
 * The game screen is a singleton view that consists of
 * a scoreboard, the gem table and TBD
 */

import animate;
import device;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;
import ui.ParticleEngine as ParticleEngine;
import src.Gem as Gem;
import src.Mermaid as Mermaid;
import src.ScoreBoard as ScoreBoard;
import src.Bubble as Bubble;
import src.FloatingScore as FloatingScore;

/* Some game constants.
 */
var score = 0,
		objective_score = 65,
		hit_value = 1,
		mole_interval = 600,
		game_on = false,
		game_length = 60000, //60 secs
		countdown_secs = game_length / 1000,
		lang = 'en';

var numOfObjective = 4;		
var total_move = 30;

var scale_ratio = device.height/1024;

var isSwapping = false;
var COL_MAX = 7;
var ROW_MAX = 7;

var col_offset = 20;
var row_offset = 405;
var col_pad = 3;
var row_pad = 3;
var directionArr = [[0, 1], [1, 0], [0, -1], [-1, 0]];

var win_image = new Image({url: "resources/images/win.png"});
var lose_image = new Image({url: "resources/images/lose.png"});
var start_image = new Image({url: "resources/images/1.png"});

var particle_img = 'resources/images/sparkly.png';
var particle_img_blue = 'resources/images/sparklyBlue.png';
var particle_img_green = 'resources/images/sparklyGreen.png';
var particle_img_orange = 'resources/images/sparklyOrange.png';
var particle_img_pink = 'resources/images/sparklyPink.png';
var particle_img_purple = 'resources/images/sparklyPurple.png';
var particle_img_red = 'resources/images/sparklyRed.png';
var particle_img_white = 'resources/images/sparklyWhite.png';
/* The GameScreen view is a child of the main application.
 * By adding the scoreboard and the gem table as it's children,
 * everything is visible in the scene graph.
 */
exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: 750,
			height: 1334,
		});

		supr(this, 'init', [opts]);

		this.build();
	};

	/*
	 * Layout the scoreboard and gem table.
	 */
	this.build = function () {
		/* The start event is emitted from the start button via the main application.
		 */
		this.on('app:start', choose_mode.bind(this));
	
		this._particleImgArr = [particle_img, particle_img_blue, particle_img_green, particle_img_orange, particle_img_orange, particle_img_pink, particle_img_purple, particle_img_red, particle_img_white];
	
		this.style.width = 750;
		this.style.height = 1334;

		this.background = new ui.ImageView({
			superview: this,
			image: "resources/images/bkgd_table.png",
			x: 0,
			y: 0,
			width: 750,
			height: 1334
		});
		
		this.gemLayer = new ui.View({
			superview: this,
			zIndex: 1,
			x: 0,
			y: 0,
			width: 750,
			height: 1334,
			opacity: 1
		});		
				
        //Set up mode selection
        this._normal_mode = new ui.TextView({
            superview: this,
            visible: false,
			zIndex: 1000,
            x: 175,
            y: 500,
            width: 400,
            height: 100,
            size: 64,
			verticalAlign: 'middle',
			horizontalAlign: 'center',
			wrap: false,
			backgroundColor: "blue",
            color: '#FFFF00',
        });
		this._normal_mode.setText("Normal");
		this._normal_mode.style.visible = false;		
		
        this._objective_mode = new ui.TextView({
            superview: this,
            visible: false,
			zIndex: 1000,
            x: 175,
            y: 650,
            width: 400,
            height: 100,
            size: 64,
			verticalAlign: 'middle',
			horizontalAlign: 'center',
			wrap: false,			
			backgroundColor: "blue",
            color: '#FFFF00',
        });
		this._objective_mode.setText("Objective");
		this._objective_mode.style.visible = false;		
		
		
		this._gameboard_glow_left = new ui.ImageView({
			superview: this,
			image: "resources/images/gameboard_glow_left.png",
			x: 0,
			y: 476,
			width: 70 * scale_ratio,
			height: 436 * scale_ratio + 1
		});
		
		this._gameboard_glow_right = new ui.ImageView({
			superview: this,
			image: "resources/images/gameboard_glow_right.png",
			x: 660,
			y: 476,
			width: 70 * scale_ratio,
			height: 436 * scale_ratio + 1
		});	
		
		this._gameboard_glow_bottom = new ui.ImageView({
			superview: this,
			image: "resources/images/gameboard_glow_bottom.png",
			x: 0,
			y: 1045,
			width: 750,
			height: 70 * scale_ratio
		});
		
		this._gameboard_glow_top = new ui.ImageView({
			superview: this,
			image: "resources/images/gameboard_glow_top.png",
			x: 0,
			y: 385,
			width: 750,
			height: 70 * scale_ratio
		});	
		
		this._score_board = new ScoreBoard();
		this.addSubview(this._score_board);
		
		this._layout = [[1, 2, 3, 2, 4, 5, 1], [3, 1, 1, 4, 4, 2, 4], [5, 1, 4, 2, 1, 4, 2], [3, 4, 5, 1, 4, 2, 1], [1, 2, 4, 5, 3, 3, 5], [3, 3, 4, 1, 2, 1, 1], [4, 5, 2, 3, 5, 2, 4]];
		this._gems = [];
		this._count = 0;
		// This is confusing. 
		// We should find a common way to define the x + y for the table
		// For position in the screen, x is col, y is row
		// Therefore, we must do the same for the table
		for (var row = 0; row < this._layout.length; row++) {
			for (var col = 0; col < this._layout[0].length; col++) {
				var gem = new Gem();
				gem.setType(this._layout[row][col]).setPosition(row, col);
				gem.setGameScreen(this);
				gem.setOffset(col_offset + col * (gem.style.height + col_pad), row_offset + row * (gem.style.width + row_pad));
				
				this.gemLayer.addSubview(gem);
				this._gems.push(gem);
			}
		};	
		
		/*
		 * The mermaid animation
		 */		
		this._mermaid = new Mermaid();
		this._mermaid.setGameScreen(this);
		this.addSubview(this._mermaid);
		this._mermaid.style.visible = false;		
		
		/*
		 * The start board to count from 1 - 3 to start the game
		 */
		// var start_image = new Image({url: "resources/images/1.png"});
		 
		 this._startboard = new ui.ImageView({
			superview: this,
			zIndex: 3,
			image: "resources/images/1.png",
			x: 75,
			y: 450,
			width: 600,
			height: 440,
		 });
		 this._startboard.style.visible = false;
		 
		 
		this._bubbles = [];
		
		var image_button_pause = new Image({url: "resources/images/button_pause.png"});
		var image_button_pause_pressed = new Image({url: "resources/images/button_pause_pressed.png"});
		this._pauseButton = new ui.ImageView({
			superview: this,
			image: image_button_pause,
			x: device.width - 115 * scale_ratio,
			y: 0,
			width: 115 * scale_ratio,
			height: 115 * scale_ratio
		});
		this._pauseButton.on("InputSelect", bind(this, function() {
			this._pauseButton.setImage(image_button_pause_pressed);
			animate(this._pauseButton).wait(50).then(bind(this, function() {
				this._pauseButton.setImage(image_button_pause);
				game_on = !game_on;
			}));
		}));
		
		this._endgameImg = new ui.ImageView({
			superview: this,
			image: "resources/images/win.png",
			zIndex: 2,
			visible: false,
			x: 100,
			y: 619,
			width: 550,
			height: 200			
		});
		
		this._EndGameParticles = [];
		
		this.on('gem:swap', bind(this, function (direction, c, r) {
			if (game_on) {
				if (isSwapping) {
					//console.log("Not doing anything!!!")
				} else {
					isSwapping = true;
					this.swap_gem(direction, c, r, true, false);
				}
			}
		}));
		
		this.on("gem:swap_done", bind(this, function(direction, c, r) {
			this.check_board_state(direction, c, r, true);
		}));
		
		this.on("gem:swap_back_finish", bind(this, function() {
			isSwapping = false;
		}));
		
		this.on("gem:drag_stop", bind(this, function() {
			//isSwapping = false;
		}));
		
		this.on("gem:disappear", bind(this, function(gem) {
			gem = null
		}));

		this.on("gamescreen:gem_fall_finish", bind (this, function(col, should_recheck_table) {
			//console.log("callback gamescreen:gem_fall_finish");
			for (var i = 0; i < ROW_MAX; i++) {				
				this._gems[i * COL_MAX + col] = this._newGems[col][ROW_MAX - i - 1];
				this._gems[i * COL_MAX + col].setPosition(i, col);
			}	
			
			this._mermaid.StartPower();
			
			if (should_recheck_table) {
				// this._count++;
				// if (this._count <= 2)
				this.check_board_state(-1, -1, -1, false);
			}
		}));
		
		this.on("gamescreen:check_board_state_done", bind (this, function() {
			//console.log("callback gamescreen:check_board_state_done");
			isSwapping = false;
			//this._mermaid.StartPower();
			if (this._currentMode == "normal") {
				this._score_board.UpdateScore(score);	
			} else {
				this._score_board.UpdateObjectives(this._objectives);
				this._score_board.update_move(--this._move);
				if (this._move <= 0)
					this.emit("move:runout");
			}
			
		}));
		
		this.on("gamescreen:bubble_disappear", bind(this, function(b) {
			//console.log("callback gamescreen:bubble_disappear");
			this.removeSubview(b);
			//this._bubbles.remove(b);	// Todo remove the bubble after it disappear
		}));
	};
	
	/* Swap 2 gem
	 */
	this.emitSparkleLoseParticles = function() {
		_winParticle = new ParticleEngine({
			parent: this._endgameImg,
			x: Math.random() * 550,
			y: Math.random() * 200,
			r: -Math.PI / 18,
			width: 100,
			height: 100,
			centerAnchor: true
		});		
		var particleObjects = _winParticle.obtainParticleArray(10);
		for (var i = 0; i < 10; i++) {
		  var pObj = particleObjects[i];
		  pObj.dx = Math.random() * 100;
		  pObj.dy = Math.random() * 100;
		  pObj.width = 60;
		  pObj.height = 60;
		  pObj.image = this._particleImgArr[0];
		}
		
		this._EndGameParticles.push(_winParticle);
		_winParticle.emitParticles(particleObjects);
	}
	
	this.emitSparkleWinParticles = function() {
		_winParticle = new ParticleEngine({
			parent: this._endgameImg,
			x: Math.random() * 550,
			y: Math.random() * 200,
			r: -Math.PI / 18,
			width: 100,
			height: 100,
			centerAnchor: true
		});		
		var particleObjects = _winParticle.obtainParticleArray(10);
		for (var i = 0; i < 10; i++) {
			var pObj = particleObjects[i];
			pObj.polar = true;
			pObj.dradius = 400;
			pObj.ddradius = -200;
			pObj.theta = i * 2 * Math.PI / 10;
			pObj.width = 60;
			pObj.height = 60;
			pObj.image = this._particleImgArr[Math.random() * 8 | 0];
		}
		
		this._EndGameParticles.push(_winParticle);
		_winParticle.emitParticles(particleObjects);
	}	
	

	
	/* Swap 2 gem
	 */
	this.swap_gem = function(direction, c, r, shouldEmit, inverse) {
		//Todo: Stop interval of gem
		//Now animate it
		
		var gem1 = this._gems[r * COL_MAX + c ];
		var gem2 = this._gems[(r + directionArr[direction - 1][0]) * COL_MAX + (c + directionArr[direction - 1][1])];
		
		// Move function related to x/y of the gem.
		// So, you must keep gem2 moving before gem1 for the reason
		// gem1 after moving will do some another things, which is check_board_statem which ends up with reset the x/y of the 2 gems
		// but then, the gem2 move animator finishes its job, moving gem2 back to unexpected position
		if (inverse == false) {
			//console.log("swap gem: " + r + " " + c +  " " + direction);
			gem2.Move(-((gem2.style.width + col_pad) * directionArr[direction - 1][0]), -((gem1.style.height + row_pad) * directionArr[direction - 1][1]), direction, false, false);
			gem1.Move((gem1.style.width + col_pad) * directionArr[direction - 1][0], (gem1.style.height + row_pad) * directionArr[direction - 1][1], direction, shouldEmit, false);
		} else {
			var invDirection = 0;
			if (direction == 1) invDirection = 3;
			if (direction == 2) invDirection = 4;
			if (direction == 3) invDirection = 1;
			if (direction == 4) invDirection = 2;
			//console.log("swap gem: " + r + " " + c +  " " + invDirection);
			gem2.Move(-((gem2.style.width + col_pad) * directionArr[invDirection - 1][0]), -((gem1.style.height + row_pad) * directionArr[invDirection - 1][1]), direction, false, false);
			gem1.Move((gem1.style.width + col_pad) * directionArr[invDirection - 1][0], (gem1.style.height + row_pad) * directionArr[invDirection - 1][1], direction, shouldEmit, true);
		}
	};
	
	/*
	*	bySwap indicates if this check is made by a swap from user
	*	= true if user just swapped two gem
	*	= false if the gems just fell and we need to check if they form matches
	*/
	this.check_board_state = function(direction, c, r, bySwap) {
	
		//console.log("enter check_board_state: c = " + c + " | r = " + r + "| direction = " + direction + " |bySwap " + bySwap);
		var that = this;

		// temporary layout 
		var layout = [];
		for (var i = 0; i < this._layout.length; i++) {
			layout.push([]);
			for (var j = 0; j < this._layout[0].length; j++)
				layout[i][j] = this._layout[i][j];
		}	
		// console.log("Before swaping");
		// for (var i = 0; i < layout.length; i++) {
			// var row_i = "";
			// for (var j = 0; j < layout[0].length; j++)
				// row_i = row_i + layout[i][j] + " ";
			// console.log(row_i);
		// }		
		
		// Swap the 2 gems temporarily on the temp board
		if (bySwap) {
			var temp = layout[r][c];
			layout[r][c] = layout[r + directionArr[direction - 1][0]][c + directionArr[direction - 1][1]];
			layout[r + directionArr[direction - 1][0]][c + directionArr[direction - 1][1]] = temp;
			// console.log("After swaping");
			// for (var i = 0; i < layout.length; i++) {
				// var row_i = "";
				// for (var j = 0; j < layout[0].length; j++)
					// row_i = row_i + layout[i][j] + " ";
				// console.log(row_i);
			// }
		}		
		/* Actually this is a swap between two same gem, so just swap it back.
		* match determines if the swap form matches, if yes, then perform it, if no, swap back
		*/
		var match = false;
		{
			for (var i = 0; i < layout.length; i++) {
				for (var j = 0; j < layout[0].length; j++)
					this._layout[i][j] = layout[i][j];
			}				
			
			// Check if there are matches by row
			for (var row = 0; row < ROW_MAX; row++) {
				var row_match_arr = [];	row_match_arr.push(0);
				var row_match = false;
				var len = 0;
				for (var i = 1; i < COL_MAX; i++) {
					if (this._layout[row][i] == this._layout[row][i-1]) {
						if (len < row_match_arr[row_match_arr.length - 1] + 1)
							len = row_match_arr[row_match_arr.length - 1] + 1;
						//console.log("i: " + i + " | val: " + this._layout[row][i] + " | len: " + len);
						row_match_arr.push(row_match_arr[row_match_arr.length - 1] + 1);
						if (len == 2) row_match = true;
					} else {
						row_match_arr.push(0);
						//console.log("i: " + i + " | val: " + this._layout[row][i] + " | len: " + 0);
					}
				}
				if (row_match == true) { 
					match = row_match;
					var temp = 0;
					for (var j = layout[row].length - 1; j >= 0; j--) {
						if (temp == 0) {
							if (row_match_arr[j] >= 2) {
								layout[row][j] = -1; temp = row_match_arr[j];
							}
						} else {
							if (row_match_arr[j] == temp - 1) {
								layout[row][j] = -1; temp = row_match_arr[j];
							}
						}
					}
				}				
			}
			
			// Check if there are matches by col
			for (var col = 0; col < COL_MAX; col++) {
				var col_match_arr = [];	col_match_arr.push(0);
				var col_match = false;
				len = 0;
				for (var i = 1; i < this._layout.length; i++) {
					if (this._layout[i][col] == this._layout[i - 1][col]) {
						if (len < col_match_arr[col_match_arr.length - 1] + 1)
							len = col_match_arr[col_match_arr.length - 1] + 1;
						//console.log("i: " + i + " | val: " + this._layout[i][col] + " | len: " + len);
						col_match_arr.push(col_match_arr[col_match_arr.length - 1] + 1);
						if (len == 2) col_match = true;					
					} else {
						col_match_arr.push(0);
						//console.log("i: " + i + " | val: " + this._layout[i][col] + " | len: " + 0);
					}
				}
				if (col_match == true) {
					match = col_match;			
					var temp = 0;
					for (var j = layout.length - 1; j >=0; j--) {
						if (temp == 0) {
							if (col_match_arr[j] >= 2) {
								layout[j][col] = -1; temp = col_match_arr[j];
							}
						} else {
							if (col_match_arr[j] == temp - 1) {
								layout[j][col] = -1; temp = col_match_arr[j];
							}
						}
					}				
				}				
			}
			
			// for (var i = 0; i < layout.length; i++) {
				// var row_i = "";
				// for (var j = 0; j < layout[0].length; j++)
					// row_i = row_i + layout[i][j] + " ";
				// console.log(row_i);
			// }			
		}
	
		if (match == false) {
			if (bySwap) {
				var temp = this._layout[r][c];			
				this._layout[r][c] = this._layout[r + directionArr[direction - 1][0]][c + directionArr[direction - 1][1]];
				this._layout[r + directionArr[direction - 1][0]][c + directionArr[direction - 1][1]] = temp;			
				// console.log("Revert layout too");
				// for (var i = 0; i < this._layout.length; i++) {
					// var row_i = "";
					// for (var j = 0; j < this._layout[0].length; j++)
						// row_i = row_i + this._layout[i][j] + " ";
					// console.log(row_i);
				// }	
				
				this.swap_gem(direction, c, r, false, true);
			}  else 
				this.emit('gamescreen:check_board_state_done');
		} else {
			if (bySwap) {
				var tempgem = this._gems[r * COL_MAX + c];
				this._gems[r * COL_MAX + c] = this._gems[(r + directionArr[direction - 1][0]) * COL_MAX + (c + directionArr[direction - 1][1])];
				this._gems[(r + directionArr[direction - 1][0]) * COL_MAX + (c + directionArr[direction - 1][1])] = tempgem;
				this._gems[r * COL_MAX + c].setPosition(r, c);
				this._gems[(r + directionArr[direction - 1][0]) * COL_MAX + (c + directionArr[direction - 1][1])].setPosition((r + directionArr[direction - 1][0]), (c + directionArr[direction - 1][1]));
			}
			
			// Clear matched gems
			// console.log("Before clearing matched gems");
			// for (var i = 0; i < layout.length; i++) {
				// var row_i = "";
				// for (var j = 0; j < layout[0].length; j++)
					// row_i = row_i + layout[i][j] + " ";
				// console.log(row_i);
			// }				
			this._newGems = [];
			for (var i = 0; i < layout[0].length; i++) {
				var count = 0;	// number of gem that matches others
				var newcol = [];
				var fallingVal = [];
				this._newGems.push([]);
				var firstRow = -1;
				for (var j = layout.length - 1; j >= 0; j--) {
					if (layout[j][i] == -1) {
						//console.log("remove gem at: " + j + " " + i);
						if (firstRow == -1) firstRow = j;
						if (this._currentMode == "objective") {
							for (var o = 0; o < numOfObjective; o++) {
								if (this._objectives[o][1] > 0) {
									//console.log("at " + o + ": " + this._objectives[o][0]  + " - " + this._objectives[o][1]);
									if (this._objectives[o][0] == this._gems[j * 7 + i]._type) {
										this._objectives[o][1] = this._objectives[o][1] - 1;
										//console.log("current obejctive need" + this._objectives[0][0] + " - " + this._objectives[0][1]);
										if (this._objectives[o][1] <= 0 && o == numOfObjective - 1) {
											//console.log("finsih all objectives");
											this.emit("objective:done");
										}									
									}
									break;
								} 
							}
						}
						this._gems[j * 7 + i].Disappear();
						//this.removeSubview(this._gems[j * 7 + i]);
						count++;
					} else {
						newcol.push(this._layout[j][i]);
						fallingVal.push(count);
						this._newGems[i].push(this._gems[j * 7 + i]);
					}
				}
				score += count;
				// Show score by swap
				//if (bySwap) {
					if (count > 0) {
						var _floating_score = new FloatingScore();
						_floating_score.style.zIndex = 1;
						this.addSubview(_floating_score);
						_floating_score.AnimateScore(count, this._gems[firstRow * 7 + i].style.x, this._gems[firstRow * 7 + i].style.y);
					}
				//}
				
				// Add new gem
				for (var k = 0; k < count; k++) {
					var ran = (Math.random() * 5 | 0) + 1;	// Hardcode 5 = number of types of gem
					//var ran = (Math.random() | 0) + 1;	// Always return 1 type of gem
					//console.log("random " + k + ": " + ran);
					newcol.push(ran);
					fallingVal.push(count);
					
					var gem = new Gem();
					gem.setType(ran);
					gem.setGameScreen(this);
					gem.setOffset(col_offset + i * (gem.style.height + col_pad), row_offset + (0 - k -1) * (gem.style.width + row_pad));
					gem.style.visible = false;
					this.gemLayer.addSubview(gem);
					this._newGems[i].push(gem);
				}
				
				if (count > 0) {
					for (var j = this._layout.length - 1; j >= 0; j--) {
						layout[j][i] = newcol[this._layout.length - j - 1];
						//console.log("layout " + j + " " + i + " :" + layout[j][i] + " will fall: " + fallingVal[j]);
					}
				}
				
				// Show score by swap
				if (bySwap) {
					if (count > 0) {
						var _floating_score = new FloatingScore();
						_floating_score.style.zIndex = 1;
						this.addSubview(_floating_score);
						_floating_score.AnimateScore(count, this._gems[firstRow * 7 + i].style.x, this._gems[firstRow * 7 + i].style.y, i, layout);
						_floating_score.on("floatingscore:done", bind(this, function(floating_score) {
							this.removeSubview(floating_score);
						}));
					}
				}				
				
				// if (count > 0) {
					// Animate gem falling by col, after each col finishes, emit gem_fall_finish,
					// When all col finishes, then check the table again
					for (var k = 0; k < this._newGems[i].length; k++) {
						this._newGems[i][k].Fall(fallingVal[k] * (this._newGems[i][k].style.width + col_pad), i, k == this._newGems[i].length - 1, i == layout[0].length - 1);
					}
				// } 
				// else {
					// this case means there are matches but not at the last col
					// still we have to notice the gamescreen that all gems has fallen
					// if (i == layout[0].length - 1)
						// this.emit("gamescreen:gem_fall_finish", i, true);
				// }
			}
				
			for (var i = 0; i < layout.length; i++) {
				for (var j = 0; j < layout[0].length; j++)
					this._layout[i][j] = layout[i][j];
			}			
			// console.log("After clearing matched gems");
			// for (var i = 0; i < this._layout.length; i++) {
				// var row_i = "";
				// for (var j = 0; j < this._layout[0].length; j++)
					// row_i = row_i + this._layout[i][j] + " ";
				// console.log(row_i);
			// }			
		}
	};
	
	this.generateBubble = function() {
		var b = new Bubble();
		b.setGameScreen(this);
		b.style.x = Math.random() * (device.width - 100);
		b.style.zIndex = 1;
		this._bubbles.push(b);
		this.addSubview(b);
		b.fly();
	};
});

/*
 * Game play
 */
 function choose_mode() {
	 this._normal_mode.style.visible = true;
	 this._objective_mode.style.visible = true;
	 this._normal_mode.once("InputSelect", start_game_flow.bind(this, "normal"));
	 this._objective_mode.once("InputSelect", start_game_flow.bind(this, "objective"));
	 
	 this.gemLayer.style.visible = false;
 }
 
 
/* Manages the intro animation sequence before starting game.
 */
function start_game_flow (mode) {
	//console.log("start_game_flow mode" + mode);
	var that = this;
	that._normal_mode.removeAllListeners('InputSelect');
	that._objective_mode.removeAllListeners('InputSelect');

	that._normal_mode.style.visible = false;
	that._objective_mode.style.visible = false;
	that.gemLayer.style.visible = true;

	animate(that.gemLayer)
			 .now({opacity: 1}, 1);	
	
	var img_start1 = new Image({url: "resources/images/1.png"});
	var img_start2 = new Image({url: "resources/images/2.png"});
	var img_start3 = new Image({url: "resources/images/3.png"});
	var img_startGo = new Image({url: "resources/images/go.png"});
	animate(that._startboard).wait(100)
		.then(function () {
			if (that._startboard.style.visible == false)
				that._startboard.style.visible = true;
			that._startboard.setImage(img_start1);
			//that._scoreboard.setText(text.READY);
		}).wait(1000).then(function () {
			that._startboard.setImage(img_start2);
			//that._scoreboard.setText(text.SET);
		}).wait(1000).then(function () {
			that._startboard.setImage(img_start3);
		}).wait(1000).then(function () {
			that._startboard.setImage(img_startGo);
			if (mode == "normal")
				that._score_board.update_countdown(game_length/1000);
			else if (mode == "objective")
				that._score_board.update_move(total_move);
		}).wait(1000).then(function () {
			//animate(that._startboard).now({x: -100, y: -100}, 1500);
			that._startboard.style.visible = false;
			that._mermaid.style.visible = true;
			//that._scoreboard.setText(text.GO);
			//start game ...
			game_on = true;
			play_game.call(that, mode);
		});
}

/* With everything in place, the actual game play is quite simple.
 * After a set timeout, proceed to the end game.
 */
function play_game (mode) {
	//console.log("play_game " + mode);
	this._currentMode = mode;
	this._score_board.Start(mode);
	// Update Mermaid animation
	//var i = setInterval(mermaidUpdate.bind(this), 100);
	
	var i = setInterval(UpdateIdleAnimation.bind(this), 40);
	var j = setInterval(UpdatePowerAnimation.bind(this), 60);
	var l = setInterval(UpdateAllBubble.bind(this), 20);
	if (mode == "normal")
		var k = setInterval(update_countdown.bind(this), 1000);
	
	if (mode == "normal") {
		setTimeout(bind(this, function () {
			game_on = false;
			clearInterval(i);
			clearInterval(j);
			clearInterval(l);
			clearInterval(k);
			setTimeout(end_game_flow.bind(this, mode, ""), mole_interval * 2);
			//this._countdown.setText(":00");
		}), game_length);
	} else if (mode == "objective") {
		this.on("objective:done", bind (this, function(){
			//console.log("objectives are done");
			game_on = false;
			clearInterval(i);
			clearInterval(j);
			clearInterval(l);
			setTimeout(end_game_flow.bind(this, mode, "win"), mole_interval * 2);
		}));
		
		this.on("move:runout", bind(this, function(){
			//console.log("run out of move");
			game_on = false;
			clearInterval(i);
			clearInterval(j);
			clearInterval(l);
			setTimeout(end_game_flow.bind(this, mode, "lose"), mole_interval * 2);			
		}));
	}

	if (mode == "objective") {
		this._move = total_move;
		this._objectives = [];
		
		for (var i = 0; i < numOfObjective; i++) {
			this._objectives.push([]);
			this._objectives[i].push((Math.random() * 5 | 0) + 1);
			this._objectives[i].push((Math.random() * 5 | 0) + 5);
			
			//console.log("objective " + i + ": " + this._objectives[i][0] + " - " + this._objectives[i][1]);
		}
		
		this._score_board.UpdateObjectives(this._objectives);
	}
	
	//Make countdown timer visible, remove start message if still there.
	// setTimeout(bind(this, function () {
		// this._scoreboard.setText(score.toString());
		// this._countdown.style.visible = true;
	// }), game_length * 0.25);

	//Running out of time! Set countdown timer red.
	// setTimeout(bind(this, function () {
		// this._countdown.updateOpts({color: '#CC0066'});
	// }), game_length * 0.75);
}

/* 
 */
function UpdateIdleAnimation () {
	this._mermaid.UpdateAnimation();
}

/* 
 */
function UpdatePowerAnimation () {
	this._mermaid.UpdatePowerAnimation();
}

/* 
 */
function UpdateAllBubble() {
	for (var i = 0; i < this._bubbles.length; i++) {
		this._bubbles[i].Update();
	}
}


/* Updates the countdown timer, pad out leading zeros.
 */
function update_countdown () {
	if (game_on) {
		countdown_secs -= 1;
		this._score_board.update_countdown(countdown_secs);
	}
	//this._countdown.setText(":" + (("00" + countdown_secs).slice(-2)));
}

function particleUpdate(lose) {
	if (lose)  {
		if (Math.random() < 0.7)
			this.emitSparkleLoseParticles();
	} else {
		if (Math.random() < 0.3)
		this.emitSparkleWinParticles();
	}
	for (var i = 0; i < this._EndGameParticles.length; i++) {
		this._EndGameParticles[i].runTick(50);
	}
}

/* Check for high-score and play the ending animation.
 * Add a click-handler to the screen to return to the title
 * screen so we may play again.
 */
function end_game_flow (mode, status) {
	//console.log("end_game_flow: " + mode);
	this._score_board.end_timer();
	this._endgameImg.style.visible = true;

	var lose = false;
	animate(this.gemLayer)
			 .now({opacity: 0.2}, 1000);
			 
	if (mode == "normal") {
		animate(this._score_board._score_view)
			.now({scale: 1.3}, 1000, animate.easeIn);

		if (score < objective_score) {
					// .wait(1000)
					// .then({opacity: 1});
			this._endgameImg.setImage(lose_image);
			lose = true;
		} else {
			this._endgameImg.setImage(win_image);
			lose = false;
		}		
	} else if (mode == "objective") {
		if (status == "lose") {
					// .wait(1000)
					// .then({opacity: 1});
			this._endgameImg.setImage(lose_image);
			lose = true;
		} else {
			this._endgameImg.setImage(win_image);
			lose = false;
		}	
	}
	
	this._WinInterval = setInterval(particleUpdate.bind(this, lose), 50);

	// slight delay before allowing a tap reset
	setTimeout(emit_endgame_event.bind(this), 2000);			
}

/* Tell the main app to switch back to the title screen.
 */
function emit_endgame_event () {
	this.once('InputSelect', function () {
		this._endgameImg.style.visible = false;
		clearInterval(this._WinInterval);
		animate(this._score_board._score_view)
			.now({scale: 1}, 1);		
		this.emit('gamescreen:end');
		reset_game.call(this);
	});
}

/* Reset game counters and assets.
 */
function reset_game () {
	score = 0;
	countdown_secs = game_length / 1000;
	this._score_board.reset_game();
	this._mermaid.reset_game();
	//this.background.clear();
}

/*
 * Strings
 */

function get_end_message (score, isHighScore) {
	var moles = (score === 1) ? text.MOLE : text.MOLES,
			end_msg = text.END_MSG_START + ' ' + score + ' ' + moles + '.\n';

	if (isHighScore) {
		end_msg += text.HIGH_SCORE + '\n';
	} else {
		//random taunt
		var i = (Math.random() * text.taunts.length) | 0;
		end_msg += text.taunts[i] + '\n';
	}
	return (end_msg += text.END_MSG_END);
}

var localized_strings = {
	en: {
		READY: "Ready ...",
		SET: "Set ...",
		GO: "Whack that Mole!",
		MOLE: "mole",
		MOLES: "moles",
		END_MSG_START: "You whacked",
		END_MSG_END: "Tap to play again",
		HIGH_SCORE: "That's a new high score!"
	}
};

localized_strings['en'].taunts = [
	"Welcome to Loserville, population: you.", //max length
	"You're an embarrassment!",
	"You'll never catch me!",
	"Your days are numbered, human.",
	"Don't quit your day job.",
	"Just press the screen, it's not hard.",
	"You might be the worst I've seen.",
	"You're just wasting my time.",
	"Don't hate the playa, hate the game.",
	"Make like a tree, and get out of here!"
];

//object of strings used in game
var text = localized_strings[lang.toLowerCase()];


