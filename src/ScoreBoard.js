import animate;
import device;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;

var scale_ratio = device.height/1024;

var numOfCoinIcon = 10;
var coin_icon = [];
var coin_icon_index = 1;
var score_number_Img = [];
var score_offsetX = 300;
var score_offsetY = 17;
var padding = 2;
var numOfObjective = 4;
var numOfTimerIcon = 14;
var time_icon = [];
var gemImg1 = new Image({url : "resources/images/icon_block_01.png"});
var gemImg2 = new Image({url : "resources/images/icon_block_02.png"});
var gemImg3 = new Image({url : "resources/images/icon_block_03.png"});
var gemImg4 = new Image({url : "resources/images/icon_block_04.png"});
var gemImg5 = new Image({url : "resources/images/icon_block_05.png"});

exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: device.width,
			height: 100
		});

		supr(this, 'init', [opts]);
		
		this._imgArr = [gemImg1, gemImg2, gemImg3, gemImg4, gemImg5];
		
		this.build();
	};

	this.build = function() {
		// this._header_score_small = new ui.ImageView({
			// superview: this,
			// image: "resources/images/header_score_small.png",
			// x: 200,
			// y: 0,
			// width: 270 * scale_ratio,
			// height: 89 * scale_ratio
		// });
		
		for (var i = 1; i <= numOfCoinIcon; i++) {
			if (i < 10)
				var imgUrl = "resources/images/coin_icon_000" + i + ".png";
			else
				var imgUrl = "resources/images/coin_icon_00" + i + ".png";
			
			coin_icon.push(new Image({url: imgUrl}));
		}
		
		for (var i = 1; i <= numOfTimerIcon; i++) {
			if (i < 10)
				var imgUrl = "resources/images/time_icon_000" + i + ".png";
			else
				var imgUrl = "resources/images/time_icon_00" + i + ".png";
			
			time_icon.push(new Image({url: imgUrl}));
		}		

		for (var i = 0; i < 10; i++)
		{	
			var imgUrl = "resources/images/numbers/score_" + i + ".png";
			score_number_Img.push(new Image({url: imgUrl}));
		}
		
        this._coin_icon = new ui.ImageView({
            superview: this,
            image: "resources/images/coin_icon_0001.png",
            x: 280,
            y: 0,
            width: 105 * scale_ratio,
            height: 105 * scale_ratio
        });
		this._coin_icon.style.visible = false;
        
		this._time_icon = new ui.ImageView({
			superview: this,
			image: "resources/images/time_icon_0001.png",
			x: 0,
			y: 0,
			width: 105 * scale_ratio,
			height: 105 * scale_ratio
		});	
		
        //Set up countdown timer
        this._countdown = new ui.TextView({
            superview: this,
            visible: false,
            x: 140,
            y: 0,
            width: 120,
            height: 120,
            size: 120,
            color: '#FFFFFF',
        });
		this._countdown.setText("");
		this._countdown.style.visible = true;
		
		this._score_view = new ui.View({
            superview: this,
            image: "resources/images/coin_icon_0001.png",
            x: 280,
            y: 0,
            width: 105 * scale_ratio,
            height: 105 * scale_ratio,
			centerAnchor: true
		});
		this._score_view.style.visible = false;
		
		this._hundred_score_image = new ui.ImageView({
			superview: this._score_view,
			image: "resources/images/numbers/score_0.png",
			x: 120,
			y: 25,
			width: 43 * scale_ratio * 0.75,
			height: 85 * scale_ratio * 0.75
		});
		
		this._teen_score_image = new ui.ImageView({
			superview: this._score_view,
			image: "resources/images/numbers/score_0.png",
			x: 160,
			y: 25,
			width: 43 * scale_ratio * 0.75,
			height: 85 * scale_ratio * 0.75
		});
		
		this._unit_score_image = new ui.ImageView({
			superview: this._score_view,
			image: "resources/images/numbers/score_0.png",
			x: 200,
			y: 25,
			width: 43 * scale_ratio * 0.75,
			height: 85 * scale_ratio * 0.75
		});
		
		this.type_icon = new ui.ImageView({
			superview: this,
			image: "resources/images/icon_block_01.png",
			x: 285,
			y: 15,
			width: 80 * scale_ratio,
			height: 80 * scale_ratio
		});	
		this.type_icon.style.visible = false;

		setInterval(update.bind(this), 100);
	};
	
	this.Start = function(mode) {
		//console.log("start with mode: " + mode);
		
		if (mode == "normal") {
			this._coin_icon.style.visible = true;
			this._score_view.style.visible = true;		
			this.type_icon.style.visible = false;
		} else {
			this._coin_icon.style.visible = false;
			this._score_view.style.visible = true;
			this.type_icon.style.visible = true;
		}
	}
	
	this.UpdateScore = function(score) {
		var hundred = parseInt(score / 100);
		var teen = parseInt((score % 100) / 10);
		var unit = (score % 100) % 10;
		//console.log("UpdateScore " + score + " " + hundred + " " + teen + " " + unit); 
		
		offsetX = score_offsetX;
		offsetY = score_offsetY;
		this._hundred_score_image.setImage(score_number_Img[hundred]);
		this._teen_score_image.setImage(score_number_Img[teen]);
		this._unit_score_image.setImage(score_number_Img[unit]);
	};
	
	this.update_countdown = function(countdown_secs) {
		this._countdown.setText(":" + (("00" + countdown_secs).slice(-2)));
		this._time_icon.setImage(time_icon[13 - countdown_secs % 14]);
	};
	
	this.update_move = function(move) {
		this._countdown.setText(move);
		this._time_icon.setImage(time_icon[0]);
	}
	
	this.end_timer = function() {
		this._countdown.setText(":00");
	}
	
	this.reset_game = function() {
		this.UpdateScore(0);
		this._countdown.setText("");
	}
	
	this.UpdateObjectives = function(objectives) {
		for (var o = 0; o < numOfObjective; o++) {
			if (objectives[o][1] > 0) {
				//console.log("at " + o + ": " + objectives[o][0]  + " - " + objectives[o][1]);
				
				this.type_icon.setImage(this._imgArr[objectives[o][0] - 1]);
				this.UpdateScore(objectives[o][1]);
				
				break;
			} 
		}		
	}
});

function update() {
	this._coin_icon.setImage(coin_icon[coin_icon_index % numOfCoinIcon]);
	
	coin_icon_index++;
}