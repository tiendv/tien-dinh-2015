import animate;
import device;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;
import src.Bubble as Bubble;

var scale_ratio = device.height/1024 * 0.6;
var numOfIdleSprites = 35;
var numOfStartPowerSprites = 12;
var numOfEndPowerSprites = 10;

var mermaid_blink = [];
var mermaid_idle = [];
var mermaid_startpower = [];
var mermaid_endpower = [];

var offsetX = 20
var offsetY = 130;

exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: offsetX,
			y: offsetY,
			width: 320 * scale_ratio,
			height: 400 * scale_ratio
		});

		supr(this, 'init', [opts]);

		this.build();
	};
	
	this.build = function() {
		this.isAnimatingIdle = 0;
		this.isAnimatingStartPower = 0;
		this.isAnimatingEndPower = 0;
		
		for (var i = 1; i <= numOfIdleSprites; i++) {
			if (i < 10)
				var imgUrl = "resources/images/mermaid_blink_000" + i + ".png";
			else
				var imgUrl = "resources/images/mermaid_blink_00" + i + ".png";
			
			mermaid_idle.push(new Image({url: imgUrl}));
		}
		
		for (var i = 1; i <= numOfStartPowerSprites; i++) {
			if (i < 10)
				var imgUrl = "resources/images/mermaid_powerstart_000" + i + ".png";
			else
				var imgUrl = "resources/images/mermaid_powerstart_00" + i + ".png";
			
			mermaid_startpower.push(new Image({url: imgUrl}));
		}
		
		for (var i = 1; i <= numOfEndPowerSprites; i++) {
			if (i < 10)
				var imgUrl = "resources/images/mermaid_powerend_000" + i + ".png";
			else
				var imgUrl = "resources/images/mermaid_powerend_00" + i + ".png";
			
			mermaid_endpower.push(new Image({url: imgUrl}));
		}		
		
		this._mermaid = new ui.ImageView({
			superview: this,
			image: "resources/images/mermaid_idle_0001.png",
			x: 0,
			y: 0,
			width: 320 * scale_ratio,
			height: 400 * scale_ratio
		});
		//this._mermaid.style.visible = false;
	};
	
	this.setGameScreen = function(gamescreen) {
		this._gameScreen = gamescreen;
	}
	
	this.PlayIdle = function() {
		if (this.isAnimatingIdle == 0)
			this.isAnimatingIdle = 1;
		
		// var start = 0;
		// this._idleInterval = setInterval(bind(this, function() {
			
		// }), 100);
		
	};
	
	this.StartPower = function() {
		if (this.isAnimatingIdle > 0)
			this.isAnimatingIdle = 0;
		
		if (this.isAnimatingStartPower == 0 && this.isAnimatingEndPower == 0)
			this.isAnimatingStartPower = 1;
	};
	
	this.UpdateAnimation = function() {
		if (this.isAnimatingIdle > 0) {
			if (this.isAnimatingIdle < numOfIdleSprites) {
				this._mermaid.setImage(mermaid_idle[this.isAnimatingIdle - 1]);
				if (this._mermaid.style.flipX)
					this._mermaid.style.x -= 4;
				else
					this._mermaid.style.x += 4;
				
				if (this._mermaid.style.x >= device.width - this._mermaid.getImage().getWidth())
					this._mermaid.style.flipX = true;
				else if (this._mermaid.style.x <= 0)
					this._mermaid.style.flipX = false;
			}
			else 
				this.isAnimatingIdle = 0;
			
			this.isAnimatingIdle++;
		}
	}
	
	this.UpdatePowerAnimation = function() {
		if (this.isAnimatingStartPower > 0) {
			if (this.isAnimatingStartPower < numOfStartPowerSprites) {
				if (this.isAnimatingStartPower == 1)
					this._gameScreen.generateBubble();

				this._mermaid.setImage(mermaid_startpower[this.isAnimatingStartPower - 1]);
				this.isAnimatingStartPower++;
			}
			else {
				this.isAnimatingStartPower = 0;
				this.isAnimatingEndPower = 1;
			}
		}
		
		if (this.isAnimatingEndPower > 0) {
			if (this.isAnimatingEndPower < numOfEndPowerSprites) {
				this._mermaid.setImage(mermaid_endpower[this.isAnimatingEndPower - 1]);
				this.isAnimatingEndPower++;
			} else {
				this.isAnimatingEndPower = 0;
			}
		}	
		
		// If there is no active animation, play idle
		if (this.isAnimatingStartPower == 0 && this.isAnimatingEndPower == 0)
			this.PlayIdle();		
	};
	
	this.reset_game = function() {
		this.style.x = 0;
		this.style.visible = false;
		this.style.flipX = false;
		this.isAnimatingIdle = 0;
		this.isAnimatingStartPower = 0;
		this.isAnimatingEndPower = 0;
	}
});