import animate;
import device;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;

var numOfbombIcon = 8;
var numOfbombPower = 4;
var bomb_icon_image = [];
var bomb_power_image = [];

exports = Class(ui.ImageView, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: device.width / 2,
			y: device.height + 80,
			image: "resources/images/bubble_icon_0001.png",
			width: 100,
			height: 100
		});

		supr(this, 'init', [opts]);
		this.style.visible = false;
		this.build();
	};
	
	this.build = function () {
		for (var i = 1; i <= numOfbombIcon; i++) {
			if (i < 10)
				var imgUrl = "resources/images/bomb_icon_000" + i + ".png";
			else
				var imgUrl = "resources/images/coin_icon_00" + i + ".png";
			
			bomb_icon_image.push(new Image({url: imgUrl}));
		}
		
		for (var i = 1; i <= numOfbombPower; i++) {
			if (i < 10)
				var imgUrl = "resources/images/bomb_power_000" + i + ".png";
			else
				var imgUrl = "resources/images/coin_power_00" + i + ".png";
			
			bomb_power_image.push(new Image({url: imgUrl}));
		}
	};
	
	this.setGameScreen = function(gs) {
		this._gameScreen = gs;
	}
	
	this.fly = function() {
		var yDes = Math.random() * 300;
		
		var count = 0;
		animate(this).now({y: yDes}, 4000, animate.linear).then(bind(this, function() {
			this.setImage(bomb_icon_image[0]);
			this._interval = setInterval(bind(this, function() {
				count++;
				if (count == numOfbombIcon + 1) {
					//console.log("Call fly end");
					clearInterval(this._interval);
					this.setImage(bomb_power_image[0]);
					var c = 0;
					this._interval = setInterval(bind(this, function() {
						c++
						if (c == numOfbombPower + 1) {
							//console.log("Call power end");
							clearInterval(this._interval);
							this._gameScreen.emit("gamescreen:bubble_disappear", this);
						} else {
							this.setImage(bomb_power_image[count]);
						}
					}), 50);
				} else {
					this.setImage(bomb_icon_image[count]);
				}
				
			}), 20);
		}));
	}
	
	this.Update = function() {
		if (this.style.y <= device.height)
			this.style.visible = true;
	}
});	