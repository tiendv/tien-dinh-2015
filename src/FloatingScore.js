import animate;
import device;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;

var coin_number_Img = [];
var scale_ratio = device.height/1024 * 0.8;

var offsetX_hundred_teen = 30;
var offsetX_teen_unit = 60;

exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: 100,
			height: 100
		});

		supr(this, 'init', [opts]);
		
		this.build();
	};
	
	this.build = function() {
		for (var i = 0; i < 10; i++)
		{	
			var imgUrl = "resources/images/numbers/coin_" + i + ".png";
			coin_number_Img.push(new Image({url: imgUrl}));
		}
		
		this._hundred_score_image = new ui.ImageView({
			superview: this,
			image: "resources/images/numbers/coin_0.png",
			x: 0,
			y: 0,
			width: 1,
			height: 1
		});
		this._hundred_score_image.style.visible = false;
		
		this._teen_score_image = new ui.ImageView({
			superview: this,
			image: "resources/images/numbers/coin_0.png",
			x: 26,
			y: 0,
			width: 1,
			height: 1
		});
		this._teen_score_image.style.visible = false;
		
		this._unit_score_image = new ui.ImageView({
			superview: this,
			image: "resources/images/numbers/coin_0.png",
			x: 52,
			y: 0,
			width: 1,
			height: 1
		});
		this._unit_score_image.style.visible = false;
	}
	
	this.AnimateScore = function(score, x, y) {
		//console.log("AnimateScore is called");
		this.style.x = x;
		this.style.y = y;
		
		var hundred = parseInt(score / 100);
		var teen = parseInt((score % 100) / 10);
		var unit = (score % 100) % 10;
		//console.log("AnimateScore " + score + " " + hundred + " " + teen + " " + unit); 

		if (hundred > 0) {
			this._hundred_score_image.style.visible = true;
			this._hundred_score_image.setImage(coin_number_Img[hundred]);
		}
		
		if (teen > 0) {
			this._teen_score_image.style.visible = true;
			this._teen_score_image.setImage(coin_number_Img[teen]);
		}
		
		this._unit_score_image.style.visible = true;
		this._unit_score_image.setImage(coin_number_Img[unit]);
		
		var count = 0;
		this._interval = setInterval(bind(this, function() {
			this._hundred_score_image.style.width = this._hundred_score_image.style.width + 1.5;
			this._hundred_score_image.style.height = this._hundred_score_image.style.height + 1.5;
			this._hundred_score_image.style.x = offsetX_hundred_teen - this._hundred_score_image.style.width;
			
			this._teen_score_image.style.width = this._teen_score_image.style.width + 1.5;
			this._teen_score_image.style.height = this._teen_score_image.style.height + 1.5;
			this._teen_score_image.style.x = offsetX_teen_unit - this._teen_score_image.style.width;
			
			this._unit_score_image.style.width = this._unit_score_image.style.width + 1.5;
			this._unit_score_image.style.height = this._unit_score_image.style.height + 1.5;
			count++;
			
			if (count == 30) {
				clearInterval(this._interval);
				animate(this).now({y: this.style.y - 80}, 600).wait(200).then(bind(this, function() {
					this.style.visible = false;
					this.emit("floatingscore:done", this);
				}));					
			}
		}), 10);
	}
});
	